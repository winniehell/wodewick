package com.ldjam.ludumdare39.wodewick.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.Arrays;
import java.util.List;

public class Wodewick extends CircleBodyActor {

    private static final float DENSITY = 1000000;
    static final int MAX_POWER = 100;
    static final float PUSH_POWER = 20;
    private static final int PUSH_VELOCITY = 100;
    public static final int MAX_LIVES = 3;
    private final Sound punchSound;
    private final Sound kissSound;

    float currentPower;
    public boolean isAttacking;
    public int numLives;
    private boolean isBeingHugged;

    @Override
    protected List<String> getTextureNames() {
        return Arrays.asList(
                "wodewick_three_lives.png",
                "wodewick_two_lives.png",
                "wodewick_one_life.png"
        );
    }

    public Wodewick(World world) {
        super(world, DENSITY);
        this.currentPower = MAX_POWER;
        this.numLives = MAX_LIVES;

        this.kissSound = Gdx.audio.newSound(Gdx.files.internal("sounds/kiss.mp3"));
        this.punchSound = Gdx.audio.newSound(Gdx.files.internal("sounds/punch.mp3"));
    }

    public void push(Direction direction) {
        if (isAttacking || (this.currentPower < PUSH_POWER)) {
            return;
        }

        this.currentPower -= PUSH_POWER;

        isAttacking = true;

        this.body.setLinearVelocity(direction.x * PUSH_VELOCITY, direction.y * PUSH_VELOCITY);

        this.punchSound.play();
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage == null) {
            return;
        }

        setPosition(getStage().getWidth() / 2, getStage().getHeight() / 2);
    }

    @Override
    public void act(float delta) {
        if (isAttacking) {
            float centerX = getStage().getWidth() / 2;
            float centerY = getStage().getHeight() / 2;
            float diffX = Math.abs(getX() - centerX);
            float diffY = Math.abs(getY() - centerY);

            if ((diffX >= getWidth()) || (diffY >= getHeight())) {
                this.physicsRadius = INITIAL_PHYSICS_RADIUS;
                this.body.setLinearVelocity(0, 0);
                setPosition(centerX, centerY);
                isAttacking = false;
            } else {
                this.physicsRadius = INITIAL_PHYSICS_RADIUS + (diffX / getWidth()) + (diffY / getHeight());
            }
        }

        this.currentPower = Math.min(this.currentPower + 2 * PUSH_POWER * delta, MAX_POWER);

        super.act(delta);
    }

    public void hug() {
        this.isBeingHugged = true;
        this.numLives -= 1;

        int textureIndex = Math.min(MAX_LIVES - this.numLives, this.textures.size() - 1);
        this.texture = this.textures.get(textureIndex);

        this.kissSound.play();
    }
}

